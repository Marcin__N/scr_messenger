#pragma once

#include <LoggerFramework/src/logger.hpp>
#include <iostream>

class BashFunctionality
{
public:

    void exitChat();
	bool invite(std::string userInvitedName);
	bool loging(std::string userName);
    void logged();
    void registered();
    void historyDownload(std::string chatHistoryNamePath);
    void fileSend(std::string fileToSendPath);
    void fileDownload(std::string fileToDownloadName);
    void displayPicture();

private:

	Logger log {SavingMode::fileOnly};

    void commandStatusLog(std::string functionName, bool commandStatus);
    bool isLogged(std::string userName);
};

namespace BashCommands
{
    static constexpr auto exitChat = "exit_chat";
    static constexpr auto invite = "invite";
    static constexpr auto logged = "logged";
    static constexpr auto historyDownload = "history_download";
    static constexpr auto fileSend = "send";
    static constexpr auto fileDownload = "download";
    static constexpr auto displayPicture = "display";
}
