#include "bashfunctionality.hpp"
#include <MessangerControl/src/chatguard.hpp>

#include <cstdlib>
#include <set>
#include <fstream>

void BashFunctionality::exitChat()
{
    log.info("BashFunctionality::exitChat started!");

    // TO DO: mbolik Tutaj musisz zrobic wyjscie z rozmowy z dana osoba
}

bool BashFunctionality::invite(std::string userInvitedName)
{
    log.info("BashFunctionality::invite started!");

	ChatGuard guardian("logged");
    if(isLogged(userInvitedName))
    {
        std::cout << userInvitedName << " is not loggged now." << std::endl;
		return false;
    }
    else
    {
        // TO DO: mbiolik Ty musisz tutaj sobie samemu to napisać, bo nie wiem na jakiej zasadzie chcesz to robić
		return true;
	}
}

bool BashFunctionality::loging(std::string userName)
{
	log.info("BashFunctionality::loging started!");

	ChatGuard guardian("logged");
	if(!isLogged(userName))
	{
		std::cout << userName << " is already loggged." << std::endl;
		return false;
	}
	else
	{
		std::fstream loggedFile("logged.txt", std::ios::app);
		userName += "\n";
		loggedFile.write(&userName[0], userName.length());
		loggedFile.close();
		return true;
	}
}

void BashFunctionality::logged()
{
    log.info("BashFunctionality::logged started!");
    std::cout << "Logged users:" << std::endl;
    bool commandStatus = system("cat $HOME/chat/configuration/logged.txt");

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::registered()
{
    log.info("BashFunctionality::registered started!");

    std::cout << "Registered users:" << std::endl;
    bool commandStatus = system("cat $HOME/chat/configuration/registered.txt");

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::historyDownload(std::string chatHistoryNamePath)
{
    log.info("BashFunctionality::historyDownload started!");

    std::string systemCommand= "cp " + chatHistoryNamePath + " $HOME/messanger/history/";       // TO DO: mnurzyns w przyszlosci trzeba ten plik usuwac po jakims czasie
    bool commandStatus = system(systemCommand.c_str());

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::fileSend(std::string fileToSendPath)
{
    log.info("BashFunctionality::fileSend started!");

    std::string systemCommand= "cp " + fileToSendPath + " $HOME/chat/files/";
    bool commandStatus = system(systemCommand.c_str());

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::fileDownload(std::string fileToDownloadName)        // ~chat/files
{
    log.info("BashFunctionality::fileDownload started!");

    std::string systemCommand= "mv $HOME/chat/files/" + fileToDownloadName + " $HOME/messanger/files/";
    bool commandStatus = system(systemCommand.c_str());

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::displayPicture()
{
    log.info("BashFunctionality::displayPicture started!");

    bool commandStatus = system("display $HOME/messanger/files/`ls $HOME/messanger/files/ -t | cut -d' ' -f1` &");

    commandStatusLog(__func__, commandStatus);
}

void BashFunctionality::commandStatusLog(std::string functionName, bool commandStatus)
{
    if(!commandStatus)
    {
        log.info((functionName + "Bash command succeed!").c_str());
    }
    else
    {
        log.info((functionName + "Bash command failes!").c_str());
        exit(3);
    }
}

bool BashFunctionality::isLogged(std::string userName)
{
    log.info("BashFunctionality::isLogged started!");

    std::set<std::string> loggedUsers;
	std::fstream loggedFile("logged.txt", std::ios::in);    //TO DO: zamiast /home/mnurzyns powinno bć $HOME, ale jak to zrobic?
    std::string dane;
    while(!loggedFile.eof())
    {
        getline(loggedFile, dane);
        loggedUsers.insert(dane);
    }
    bool isUserLogged = (loggedUsers.find(userName) == loggedUsers.end());

    return isUserLogged;
}
