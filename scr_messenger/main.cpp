#include <MessagesFramework/src/textmessage.hpp>
#include <LoggerFramework/src/logger.hpp>
#include <MessangerControl/src/communicator.hpp>
#include<BashSimulator/src/bashfunctionality.hpp>

#include <iostream>
#include <memory>

namespace constant {

const std::string my_file_with_chat { "Piotr" };
const std::string clinet_file_with_chat { "Pawel" };
const std::uint32_t id_1 { 0x1011 };
const std::uint32_t id_2 { 0x1012 };
const std::string password_1 { "HaSlo" };
const std::string password_2 { "HAslO" };

}

void marek()
{
	BashFunctionality b;
	std::string me;
	do {
		std::cout << "Podaj swój login" << std::endl;
		std::cin >> me;
	} while(!b.loging(me));
	std::string receiver;
	do {
		std::cout << "Z kim chcesz rozmawiać?" << std::endl;
		std::cin >> receiver;
	} while(!b.invite(receiver));
	Communicator comunicator(std::make_unique<User>(me, constant::id_1, constant::password_1),
	                         std::make_unique<User>(receiver, constant::id_2, constant::password_2));
}

void marcin()
{
    Logger log(SavingMode::withConsole);
    log.info("Napis dla kolejnej proby");
    BashFunctionality b;
    b.historyDownload("Logger_default.txt");
    b.displayPicture();
    b.logged();
    b.registered();
    b.invite("mpyzalsk");
    b.invite("mnurzyns");
}

enum class Run
{
    Marcin,
    Marek,
    Oba
};

int main()
{
	Run version = Run::Marek;

    if (Run::Marek == version)
    {
        marek();
    }
    else if (Run::Marcin == version)
    {
        marcin();
    }
    else if (Run::Oba == version)
    {
        marcin();
        std::cout << std::endl << std::endl << std::endl;
        marek();
    }

    return 0;
}
