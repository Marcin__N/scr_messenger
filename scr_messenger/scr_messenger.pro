 QT -= gui

CONFIG += c++14 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS_WARN_ON += -Wpedantic

#BashSimulator/
# SOURCES_BASHSIMULATOR
SOURCES += BashSimulator/src/bash.cpp \
    BashSimulator/src/bashfunctionality.cpp
SOURCES +=
# HEADERS_BASHSIMULATOR
HEADERS += BashSimulator/src/bash.hpp \
    BashSimulator/src/bashfunctionality.hpp
HEADERS +=

#MessangerControl/
# SOURCES_MESSANGERCONTROL
SOURCES += MessangerControl/src/chatguard.cpp
SOURCES += MessangerControl/src/communicator.cpp
SOURCES += MessangerControl/src/registry.cpp
SOURCES += MessangerControl/src/user.cpp
SOURCES += MessangerControl/src/userchat.cpp
SOURCES += MessangerControl/src/consoloutput.cpp
# HEADERS_MESSANGERCONTROL
HEADERS += MessangerControl/src/chatguard.hpp
HEADERS += MessangerControl/src/communicator.hpp
HEADERS += MessangerControl/src/chatguard.hpp
HEADERS += MessangerControl/src/registry.hpp
HEADERS += MessangerControl/src/user.hpp
HEADERS += MessangerControl/src/userchat.hpp
HEADERS += MessangerControl/src/consoloutput.hpp

#MessagesFramework/
# SOURCES_MESSAGESFRAMEWORK
SOURCES += MessagesFramework/src/filemessage.cpp
SOURCES += MessagesFramework/src/messagesender.cpp
SOURCES += MessagesFramework/src/messagemanager.cpp
SOURCES += MessagesFramework/src/message.cpp
SOURCES += MessagesFramework/src/textmessage.cpp
SOURCES += MessagesFramework/src/messagereceiver.cpp
SOURCES += MessagesFramework/src/messagedecorator.cpp
# HEADERS_MESSAGESFRAMEWORK
HEADERS += MessagesFramework/src/messagedecorator.hpp
HEADERS += MessagesFramework/src/filemessage.hpp
HEADERS += MessagesFramework/src/messagesender.hpp
HEADERS += MessagesFramework/src/messagemanager.hpp
HEADERS += MessagesFramework/src/message.hpp
HEADERS += MessagesFramework/src/textmessage.hpp
HEADERS += MessagesFramework/src/messagereceiver.hpp

#LoggerFramework/
# SOURCES_LOGGERFRAMEWORK
SOURCES += LoggerFramework/src/logger.cpp
# HEADERS_LOGGERFRAMEWORK
HEADERS += LoggerFramework/src/logger.hpp

#All sources adn headers
SOURCES += main.cpp
