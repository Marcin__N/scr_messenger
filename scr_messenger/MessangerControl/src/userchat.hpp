#pragma once

#include"registry.hpp"
#include "communicator.hpp"
#include <BashSimulator/src/bash.hpp>

#include <memory>

class UserChat
{
public:

    // konstruktory destruktory
    UserChat();

    void login();                       // loguje jesli nie ma w pliku to rejestruje używa klasy registry
    void inviteToChat();                // zaprasza do czatu
    void joinToChat();                  // łączenie z czatem po zaproszeniu
    void bashCommand();                 // uzywa publicznego interfejsu komend bashowych, wywołuje interpretor
    void endChat();                     // wyłącza czat itd.
    void saveHistory();                 // zapisuje historię czatu

private:

    const std::unique_ptr<User> user;
    const std::unique_ptr<Registry> registryManager;
    const std::unique_ptr<Communicator> communicator;
    const std::unique_ptr<Bash> bash;
    const std::string ID;
    const std::string password;

    void registration();
    void logout();

};
