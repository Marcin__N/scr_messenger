#pragma once

//#include <MessagesFramework/src/messagemanager.hpp>
#include "user.hpp"
#include "consoloutput.hpp"
#include <MessagesFramework/src/messagereceiver.hpp>
#include <MessagesFramework/src/messagesender.hpp>
#include <LoggerFramework/src/logger.hpp>

#include <memory>

class Communicator
{
public:
	Communicator(std::unique_ptr<User> me, std::unique_ptr<User> second_user);

private:
	std::shared_ptr<ConsolOutPut> consola { std::make_shared<ConsolOutPut>() };
	Logger test { SavingMode::withConsole };

};

