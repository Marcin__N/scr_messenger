#pragma once

#include"registry.hpp"

#include <string>

class User
{
public:
	User(std::string name_, uint32_t id_, std::string password_);
	~User();

	std::string get_name() const;

private:
	const std::string name;
	const std::uint32_t id;
	const std::string password;
};
