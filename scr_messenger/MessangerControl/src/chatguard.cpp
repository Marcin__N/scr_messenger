#include "chatguard.hpp"

#include <chrono>
#include <fstream>
#include <iostream>
#include <thread>

ChatGuard::ChatGuard(std::string file_name_):
    guardFileName(file_name_ + "_guard")
{
	while (std::ifstream(guardFileName)) {
		std::cerr << "File already exists" << guardFileName << std::endl;
		std::cerr << "(Pewnie źle zakończyłeś program... usun go teraz recznie)" << std::endl;
	}
	std::ofstream guard_file(guardFileName.c_str());
}

ChatGuard::~ChatGuard()
{
	std::remove(guardFileName.c_str());
}
