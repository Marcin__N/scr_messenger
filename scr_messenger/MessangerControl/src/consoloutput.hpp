#pragma once

#include <mutex>
#include <string>

class ConsolOutPut
{
public:
	ConsolOutPut() = default;
	ConsolOutPut(ConsolOutPut&&) = delete;
	ConsolOutPut& operator=(ConsolOutPut&&) = delete;
	ConsolOutPut(const ConsolOutPut&);
	ConsolOutPut& operator=(const ConsolOutPut&);

	void sender_write_on_consol(std::string text);
	void receiver_write_on_consol(std::string text);

private:
	void write_on_consol(std::string text);

	std::mutex consolMutex;
	std::string whole_text {""};

};
