#pragma once

#include "user.hpp"

#include <fstream>
#include <iostream>
#include <memory>

class Registry
{
public:

    Registry();
    void isLogged();                // przeszukująca czy podany użytkownik jest zalogowany
    void loginUser();               // loguje użytkownika do czatu


private:

    std::unique_ptr<std::fstream> registeredUsers;      // wskaznik na plik zarejestrowanych uzytkownikow
    std::unique_ptr<std::fstream> loggedUsers;          // wskaznik na plik zalogowanych uzytkownikow

    void registerUser();            // rejestruje użytkownika
};
