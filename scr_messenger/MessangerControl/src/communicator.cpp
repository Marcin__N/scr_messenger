#include "communicator.hpp"

Communicator::Communicator(std::unique_ptr<User> me, std::unique_ptr<User> second_user)
{
	std::string my_name { me->get_name() };
	MessageReceiver receiver(std::move(me), consola);
	std::cerr << "Error: can't open file: " << std::endl;
	MessageSender sender(my_name, std::move(second_user), consola);
}
