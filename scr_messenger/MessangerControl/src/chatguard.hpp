#ifndef CHATGUARD_H
#define CHATGUARD_H

#include <string>

class ChatGuard
{
public:
	ChatGuard(std::string file_name);
	~ChatGuard();

private:
	std::string guardFileName;

};

#endif // CHATGUARD_H
