#include "consoloutput.hpp"

#include <cstdio>
#include <iostream>
#include <memory>

namespace colors {

constexpr std::uint32_t red { 31 };
constexpr std::uint32_t yellow { 33 };
constexpr std::uint32_t blue { 34 };
constexpr std::uint32_t white { 37 };
constexpr std::uint32_t green { 32 };

} // namespace colors

ConsolOutPut::ConsolOutPut(const ConsolOutPut& right)
{
	this->whole_text = right.whole_text;
}

ConsolOutPut& ConsolOutPut::operator = (const ConsolOutPut& right)
{
	this->whole_text = right.whole_text;
	return *this;
}

void ConsolOutPut::write_on_consol(std::string text)
{
	whole_text += text;

	system("clear");

	std::cout << whole_text << std::endl;
}

void ConsolOutPut::sender_write_on_consol(std::string text)
{
	std::unique_lock<std::mutex> lock { consolMutex };
	printf("%c[%dm",0x1B,colors::blue);
	write_on_consol(text);
	printf("%c[%dm",0x1B,colors::white);
}

void ConsolOutPut::receiver_write_on_consol(std::string text)
{
	std::unique_lock<std::mutex> lock { consolMutex };
	printf("%c[%dm",0x1B,colors::red);
	text += '\n';
	write_on_consol(text);
	printf("%c[%dm",0x1B,colors::white);
}
