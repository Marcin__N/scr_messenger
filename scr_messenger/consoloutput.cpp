#include "consoloutput.hpp"

#include <iostream>

#define RED 31
#define YELLOW 33
#define BLUE 34
#define WHITE 37
#define GREEN 32

void write_on_consol(std::string text)
{
	static std::string whole_text;

	whole_text += text;

	system("clear");

	std::cout << whole_text;
}

void sender_write_on_consol(std::string text)
{
	printf("%c[%dm",0x1B,BLUE);
	write_on_consol(text);
	printf("%c[%dm",0x1B,WHITE);
}

void receiver_write_on_consol(std::string text)
{
	printf("%c[%dm",0x1B,RED);
	text += '\n';
	write_on_consol(text);
	printf("%c[%dm",0x1B,WHITE);
}
