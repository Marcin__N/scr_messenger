#ifndef CONSOLOUTPUT_H
#define CONSOLOUTPUT_H

#include <string>

void write_on_consol(std::string text);
void sender_write_on_consol(std::string text);
void receiver_write_on_consol(std::string text);

#endif // CONSOLOUTPUT_H
