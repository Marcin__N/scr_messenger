#include "messagereceiver.hpp"
#include <MessangerControl/src/chatguard.hpp>
#include <MessangerControl/src/user.hpp>

#include <chrono>
#include <iostream>
#include <memory>

namespace constant {

constexpr std::chrono::milliseconds waitTime { 200 };
//constexpr std::chrono::milliseconds waitTime2 { 20 };

}

MessageReceiver::MessageReceiver(std::unique_ptr<User> user, std::shared_ptr<ConsolOutPut> consola_):
    receiverFileName(user->get_name()),
    consola(consola_)
{
	test.info("constructor");
	isReceiving = true;
	receivingThread = std::thread(&MessageReceiver::receiveMessages, this);
	std::unique_lock<std::mutex> lock { dispatcherLock };
	initializeThreadCondition.wait(lock, [&](){ return euid != 0; });
}

MessageReceiver::~MessageReceiver()
{
	isReceiving = false;
	receivingThread.join();
}

void MessageReceiver::initialize()
{
	ChatGuard guardian(receiverFileName); // TODO: chnage to unique
	moveOldChatToHistory(); // TODO: mnurzyns
	cleanUpFile();
}

void MessageReceiver::moveOldChatToHistory()
{
	// TODO: write it
}

void MessageReceiver::cleanUpFile()
{
	std::remove(receiverFileName.c_str());
	receiverFile.open(receiverFileName, std::ios::out);
	receiverFile.close();
}

void MessageReceiver::moveMessageToHistory()
{
	// TODO: write it
}

void MessageReceiver::receiveMessages()
{
	test.info("start receiveing");
	initialize();
	euid = 1;
	initializeThreadCondition.notify_one();
	while (isReceiving) {
		basic_message message { receiveMessage() };
		if (message != "") {
			consola->receiver_write_on_consol(message);
			test.info("receive message");
		}
		test.info("nothing");
		std::this_thread::sleep_for(constant::waitTime);
	}
}

MessageReceiver::basic_message MessageReceiver::receiveMessage()
{
	ChatGuard guardian(receiverFileName); // TODO: chnage to unique
	receiverFile.open(receiverFileName, std::ios::in);
	basic_message message;
	if (!receiverFile.good())
	{
		//std::cerr << "Error: can't open file receive!: " << receiverFileName << std::endl;
	}
	else {
		getline(receiverFile, message );
		cleanUpFile();
		moveMessageToHistory();
		receiverFile.close();
	}
	return message;
}
