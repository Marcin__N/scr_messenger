#include "textmessage.hpp"

#include <chrono>
#include <ctime>

TextMessage::TextMessage(std::string sender_, std::string receiver_, std::string text_):
    Message(sender_, receiver_),
    text(text_)
{

}

std::string TextMessage::getDate()
{
	auto time { std::chrono::system_clock::now() };
	std::time_t send_time = std::chrono::system_clock::to_time_t(time);
	std::string s_time { std::ctime(&send_time) }; // hakerka...
	s_time.resize(s_time.size()-1);
	std::stringstream ss;
	ss << "[" << sender << "][" <<
	      s_time << "]" <<
	      text << "\n";
	//std::cout << ss.str(); // TODO: remove
	return ss.str();
}
