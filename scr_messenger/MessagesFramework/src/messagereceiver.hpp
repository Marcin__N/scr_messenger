#pragma once

#include <MessangerControl/src/consoloutput.hpp>
#include <LoggerFramework/src/logger.hpp>

#include <condition_variable>
#include <fstream>
#include <mutex>
#include <thread>

#define NO_MOVE_CONSTRUCTOR(class) \
	class(class&&) = delete
#define NO_MOVE_ASSIGNMENT(class) \
	class& operator=(class&&) = delete
#define NO_MOVE(class) \
	NO_MOVE_CONSTRUCTOR(class); \
	NO_MOVE_ASSIGNMENT(class)

#define NO_COPY_CONSTRUCTOR(class) \
	class(const class&) = delete
#define NO_COPY_ASSIGNMENT(class) \
	class& operator=(const class&) = delete
#define NO_COPY(class) \
	NO_COPY_CONSTRUCTOR(class); \
	NO_COPY_ASSIGNMENT(class)

#define NO_MOVE_NOR_COPY(class) \
	NO_MOVE(class); \
	NO_COPY(class)

class User;

class MessageReceiver
{
public:
	MessageReceiver(std::unique_ptr<User> user, std::shared_ptr<ConsolOutPut> consola_);
	~MessageReceiver();

	NO_MOVE_NOR_COPY(MessageReceiver);

private:
	using basic_message = std::string;
	void initialize();
	void lockFile();
	void moveOldChatToHistory();
	void cleanUpFile();
	void freeFile();
	void moveMessageToHistory();
	void receiveMessages();
	basic_message receiveMessage();

	std::thread receivingThread;
	std::uint32_t euid { 0 }; // TODO: chnage to boost optional or std
	bool isReceiving { false };
	std::condition_variable initializeThreadCondition;
	std::mutex dispatcherLock;
	std::string receiverFileName;
	std::fstream receiverFile;
	std::shared_ptr<ConsolOutPut> consola;

	Logger test { SavingMode::fileOnly };

};
