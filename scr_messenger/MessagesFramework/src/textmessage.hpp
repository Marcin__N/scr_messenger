#pragma once

#include "message.hpp"

#include <sstream>
#include <iostream> // TODO: remove

class TextMessage: public Message
{
public:
	TextMessage(std::string sender_, std::string receiver_, std::string text_);
	~TextMessage() = default;

	std::string getDate();

private:
	std::string text;
};
