#pragma once

#include <string>

class Message
{
public:
	Message(std::string sender_, std::string receiver_);
	virtual ~Message(){}

protected:
	//std::string time;
	std::string sender;
	std::string receiver;

};
