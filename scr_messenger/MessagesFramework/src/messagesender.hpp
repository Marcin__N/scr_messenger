#pragma once

#include "textmessage.hpp"
#include <MessangerControl/src/consoloutput.hpp>
#include <LoggerFramework/src/logger.hpp>

#include <fstream>
#include <memory>

class User;

class MessageSender
{
public:
	MessageSender(std::string sender_, std::unique_ptr<User> receiver, std::shared_ptr<ConsolOutPut> consola_);

	void sendMessage(TextMessage message);

private:
	void sendMessages();
	void sendMessage();

	std::string sender;
	std::unique_ptr<User> receiver;
	std::fstream receiver_file;
	std::shared_ptr<ConsolOutPut> consola;

	Logger test { SavingMode::fileOnly };
};
