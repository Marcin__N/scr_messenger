#include "messagesender.hpp"
#include <MessangerControl/src/chatguard.hpp>
#include <MessangerControl/src/user.hpp>

#include <iostream> // TODO

MessageSender::MessageSender(std::string sender_, std::unique_ptr<User> receiver_, std::shared_ptr<ConsolOutPut> consola_):
    sender(sender_),
    receiver(std::move(receiver_)),
    consola(consola_)
{
	test.info("constructor: sender");
	sendMessages();
}

void MessageSender::sendMessages()
{
	std::string text { "wpisz 0 zeby przerwac\n" };
	consola->sender_write_on_consol(text);
	do {
		getline(std::cin, text);
		if (text == "0") {
			text = sender + " is logged out\n";
			TextMessage first(sender, receiver->get_name(), text);
			sendMessage(first);
			text = "0";
		}
		else {
			TextMessage first(sender, receiver->get_name(), text);
			sendMessage(first);
		}
	} while (text != "0");
}

void MessageSender::sendMessage(TextMessage message)
{
	ChatGuard guardian(receiver->get_name()); // TODO: chnage to unique
	receiver_file.open(receiver->get_name(), std::ios::app);
	if (!receiver_file.good())
	{
		std::cerr << "Error: can't open file: " << receiver->get_name() << std::endl;
	}
	std::string text { message.getDate() };
	consola->sender_write_on_consol(text);
	receiver_file.write(&text[0], text.length());
	receiver_file.close();
}
