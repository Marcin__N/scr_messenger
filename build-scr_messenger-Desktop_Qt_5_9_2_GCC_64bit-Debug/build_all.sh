#!/bin/bash

rm Logger_default.txt
make clean logger.o

echo "Building process START!"
echo

qmake ../scr_messenger/scr_messenger.pro -spec linux-g++ CONFIG+=debug

echo
echo "QMAKE process DONE"
echo

make

echo
echo "Makefile process DONE!"
echo
echo "Building process END!"
